function initialize() {

    // création de la carte et paramétrage général : centre , niveau de zoom, FullScreen

    var map = L.map('map', {
        fullscreenControl: {
            pseudoFullscreen: false
        }
    }).setView([14.446180, -2.24039], 10.4);

    //Ajout de la règle pour le calcul de distance
    L.control.ruler().addTo(map);

    // création d'une couche "osmLayer"
    var osmLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '© <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
        maxZoom: 19
    });


    //Création d'une couche "openMapSurfer_Roads": Routes
    var openMapSurfer_Roads = L.tileLayer('https://korona.geog.uni-heidelberg.de/tiles/roads/x={x}&y={y}&z={z}', {
        maxZoom: 20,
        attribution: 'Imagery from <a href="http://giscience.uni-hd.de/">GIScience Research Group @ University of Heidelberg</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });


    //Création d'une couche "esri_WorldImagery" : Satellite
    var esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
    });


    // la couche "osmLayer" est ajoutée à la carte
    map.addLayer(osmLayer);


    var controlSearch = new L.Control.Search({
        position: 'topleft',
        layer: osmLayer,
        initial: false,
        zoom: 11,
        marker: false,
        textPlaceholder: 'search...'
    });

    map.addControl(controlSearch);

    controlSearch.on('search:collapsed', function (e) {
        map.setView([51.101516, 10.313446], 6);
    });

// add location control to global name space for testing only
// on a production site, omit the "lc = "!
    lc = L.control.locate({
        strings: {
            title: "Votre position"
        }
    }).addTo(map);

    // création d'une couche geoJson qui appelle le fichier "villages.geojson"													
    var villages = $.getJSON("villages.geojson", function (dataVillages)
            // icone villages	
            {
                var iconeVillage = L.icon({
                    iconUrl: 'images/village.png',
                    iconSize: [32, 32]
                });
                // fonction pointToLayer qui ajoute la couche "villages" à la carte, selon la symbologie "iconeVillage", et paramètre la popup
                L.geoJson(dataVillages, {
                    pointToLayer: function (feature, latlng) {
                        var marker = L.marker(latlng, {icon: iconeVillage});
                        marker.bindPopup('<b><u>Description du Village</u></b><br>'
                                + "<b>Nom : </b>" + feature.properties.Village + '<br>'
                                + "<b>Nombre d'habitants : </b>" + feature.properties.Demography + '<br>'
                                + "<b>Besoin en eau : </b>" + feature.properties.M_water_re
                                );
                        return marker;
                    }
                }).addTo(map);
            });




    // création d'une couche geoJson qui appelle le fichier "wells.geojson"													
    var wells = $.getJSON("wells.geojson", function (dataWells)
            // icone villages	
            {
                var iconeWell = L.icon({
                    iconUrl: 'images/well4.png',
                    iconSize: [15, 20]
                });
                // fonction pointToLayer qui ajoute la couche "villages" à la carte, selon la symbologie "iconeVillage", et paramètre la popup
                L.geoJson(dataWells, {
                    pointToLayer: function (feature, latlng) {
                        var marker = L.marker(latlng, {icon: iconeWell});
                        marker.bindPopup('<b><u>Description du puits</u></b><br>'
                                + "<b>Quantité d'eau : </b>" + feature.properties.Well_water + '<br>'
                                + "<b>Village bénéficiaire : </b>" + feature.properties.join_Villa + '<br>'
                                + "<b>Distance entre le puits et le village " + feature.properties.join_Villa + ": </b>" + feature.properties.distance
                                );
                        return marker;
                    }
                }).addTo(map);
            });



    // création d'un contrôle des couches pour modifier les couches de fond de plan	
    var baseLayers = {
        "OpenStreetMap": osmLayer,
        "Couche des routes": openMapSurfer_Roads,
        "Couche satellitaire": esri_WorldImagery
    };
    L.control.layers(baseLayers).addTo(map);

}