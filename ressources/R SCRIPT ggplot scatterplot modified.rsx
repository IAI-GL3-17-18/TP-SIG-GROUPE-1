##Vector processing=group
##showplots
##Layer=vector
##X=Field Layer
##Y=Field Layer
##LABEL=Field Layer
##Group=Field Layer
require(ggplot2)
ggplot()+
geom_point(aes(x=Layer[[X]],y=Layer[[Y]],
label=as.character(Layer[[LABEL]]), size=7,
color=as.factor(Layer[[Group]])))+
theme(legend.title = element_blank())+
xlab(X)+
ylab(Y)+
geom_text(aes(x=Layer[[X]],y=Layer[[Y]],label=as.character(Layer[[LABEL]])),hjust=0, vjust=0, size=5)+
geom_abline(slope=1)+
ggtitle("Demande en eau par village en fonction \nde l'eau disponible par village")+
theme(plot.title = element_text(lineheight=.8, face="bold"))
